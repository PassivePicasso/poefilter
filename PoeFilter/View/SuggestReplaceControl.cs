﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Commander;

namespace PoeFilter.View
{
    public enum MatchMode { StartsWith, Contains, EndsWith };
    /// <summary>
    /// Acquired from http://blog.pixelingene.com/2010/10/tokenizing-control-convert-token-to-tokens/
    /// </summary>
    public class SuggestReplaceControl : TextBox
    {

        public static readonly DependencyProperty SuggestionsProperty = DependencyProperty.Register("Suggestions", typeof(IList), typeof(SuggestReplaceControl), new FrameworkPropertyMetadata(default(IList), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty SuggestionOptionsProperty = DependencyProperty.Register("SuggestionOptions", typeof(IList), typeof(SuggestReplaceControl), new FrameworkPropertyMetadata(default(IList), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty SuggestionMemberProperty = DependencyProperty.Register("SuggestionMember", typeof(String), typeof(SuggestReplaceControl), new FrameworkPropertyMetadata(default(String), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty MatchModeProperty = DependencyProperty.Register("MatchMode", typeof(MatchMode), typeof(SuggestReplaceControl), new FrameworkPropertyMetadata(default(MatchMode), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty HasSuggestionsProperty = DependencyProperty.Register("HasSuggestions", typeof(bool), typeof(SuggestReplaceControl), new PropertyMetadata(default(bool)));
        public static readonly DependencyProperty SuggestionRectProperty = DependencyProperty.Register("SuggestionRect", typeof(Rect), typeof(SuggestReplaceControl), new PropertyMetadata(default(Rect)));

        #region Properties

        public IList Suggestions
        {
            get { return (IList)GetValue(SuggestionsProperty); }
            set { SetValue(SuggestionsProperty, value); }
        }

        public IList SuggestionOptions
        {
            get { return (IList)GetValue(SuggestionOptionsProperty); }
            set { SetValue(SuggestionOptionsProperty, value); }
        }

        public String SuggestionMember
        {
            get { return (String)GetValue(SuggestionMemberProperty); }
            set { SetValue(SuggestionMemberProperty, value); }
        }

        public bool HasSuggestions
        {
            get { return (bool)GetValue(HasSuggestionsProperty); }
            set { SetValue(HasSuggestionsProperty, value); }
        }
        public MatchMode MatchMode
        {
            get { return (MatchMode)GetValue(MatchModeProperty); }
            set { SetValue(MatchModeProperty, value); }
        }

        public Rect SuggestionRect
        {
            get { return (Rect)GetValue(SuggestionRectProperty); }
            set { SetValue(SuggestionRectProperty, value); }
        }

        public bool CaseSensitive
        {
            get { return (bool)GetValue(CaseSensitiveProperty); }
            set { SetValue(CaseSensitiveProperty, value); }
        }

        #endregion

        private PropertyInfo SelectionMemberGetter;
        public static readonly DependencyProperty CaseSensitiveProperty = DependencyProperty.Register("CaseSensitive", typeof(bool), typeof(SuggestReplaceControl), new PropertyMetadata(default(bool)));

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            SetCurrentValue(HasSuggestionsProperty, false);
            SetCurrentValue(SuggestionsProperty, new List<object>());
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (SuggestionOptions == null || SuggestionOptions.Count == 0) return;
            if (SelectionMemberGetter == null && !string.IsNullOrEmpty(SuggestionMember))
                SelectionMemberGetter = SuggestionOptions[0].GetType().GetProperty(SuggestionMember);

            if (SelectionMemberGetter == null)
            {
                var data = new object[SuggestionOptions.Count];
                SuggestionOptions.CopyTo(data, 0);
                var suggests = data.Where(so =>
                {
                    switch (MatchMode)
                    {
                        case MatchMode.StartsWith:
                            return CaseSensitive ? so.ToString().StartsWith(Text) : so.ToString().ToLower().StartsWith(Text.ToLower());
                        case MatchMode.Contains:
                            return CaseSensitive ? so.ToString().Contains(Text) : so.ToString().ToLower().Contains(Text.ToLower());
                        case MatchMode.EndsWith:
                            return CaseSensitive ? so.ToString().EndsWith(Text) : so.ToString().ToLower().EndsWith(Text.ToLower());
                    }
                    return false;
                }).ToList();

                SetCurrentValue(SuggestionsProperty, suggests);
                SetCurrentValue(HasSuggestionsProperty, suggests.Any());

                SetCurrentValue(SuggestionRectProperty, GetRectFromCharacterIndex(0, false));
                return;
            }
            if (Suggestions == null)
                SetCurrentValue(SuggestionsProperty, new List<object>());
            else
            {
                var data = new object[SuggestionOptions.Count];
                SuggestionOptions.CopyTo(data, 0);
                var suggests = data.Where(so =>
                {
                    switch (MatchMode)
                    {
                        case MatchMode.StartsWith:
                            return CaseSensitive ? SelectionMemberGetter.GetValue(so, null).ToString().StartsWith(Text)
                                                 : SelectionMemberGetter.GetValue(so, null).ToString().ToLower().StartsWith(Text.ToLower());
                        case MatchMode.Contains:
                            return CaseSensitive ? SelectionMemberGetter.GetValue(so, null).ToString().Contains(Text)
                                                 : SelectionMemberGetter.GetValue(so, null).ToString().ToLower().Contains(Text.ToLower());
                        case MatchMode.EndsWith:
                            return CaseSensitive ? SelectionMemberGetter.GetValue(so, null).ToString().EndsWith(Text)
                                                 : SelectionMemberGetter.GetValue(so, null).ToString().ToLower().EndsWith(Text.ToLower());
                    }
                    return false;
                }).ToList();

                SetCurrentValue(SuggestionsProperty, suggests);
                SetCurrentValue(HasSuggestionsProperty, suggests.Any());

                SetCurrentValue(SuggestionRectProperty, GetRectFromCharacterIndex(0, false));
            }
        }

        [OnCommand("ReplaceCommand")]
        public void ReplaceLastRunWithText(string text)
        {
            if(string.IsNullOrEmpty(text)) return;
            
            // Add text to the text
            SetCurrentValue(TextProperty, text);

            SetCurrentValue(SuggestionsProperty, new List<object>());
            SetCurrentValue(HasSuggestionsProperty, false);

            // Move focus back to the text box. 
            // This will auto-hide the PopUp due to StaysOpen="false"
            Focus();
        }
    }
}