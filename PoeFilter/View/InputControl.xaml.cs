﻿using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace PoeFilter.View
{
    /// <summary>
    /// Interaction logic for InputControl.xaml
    /// </summary>
    public partial class InputControl
    {
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IList), typeof(InputControl), new PropertyMetadata(default(IList)));
        public static readonly DependencyProperty LabelProperty = DependencyProperty.Register("Label", typeof(string), typeof(InputControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof (string), typeof (InputControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty SetProperty = DependencyProperty.Register("Set", typeof(bool), typeof(InputControl), new PropertyMetadata(default(bool)));
        public static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground", typeof (object), typeof (InputControl), new PropertyMetadata(default(object)));
        public static readonly DependencyProperty BorderBrushProperty = DependencyProperty.Register("BorderBrush", typeof (object), typeof (InputControl), new PropertyMetadata(default(object)));
        public static readonly DependencyProperty BorderThicknessProperty = DependencyProperty.Register("BorderThickness", typeof (object), typeof (InputControl), new PropertyMetadata(default(Thickness)));


        public InputControl()
        {
            InitializeComponent();

        }

        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public bool Set
        {
            get { return (bool)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public object Foreground
        {
            get { return (object) GetValue(ForegroundProperty); }
            set { SetValue(ForegroundProperty, value); }
        }

        public object BorderBrush
        {
            get { return (object) GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }

        public object BorderThickness
        {
            get { return (object) GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }

        private void ListBoxUp(object sender, ExecutedRoutedEventArgs e)
        {
            SuggestionListBox.SelectedIndex--;
            SuggestionListBox.InvalidateVisual();
            SuggestionListBox.ScrollIntoView(SuggestionListBox.SelectedItem);
        }

        private void ListBoxDown(object sender, ExecutedRoutedEventArgs e)
        {
            SuggestionListBox.SelectedIndex++;
            SuggestionListBox.InvalidateVisual();
            SuggestionListBox.ScrollIntoView(SuggestionListBox.SelectedItem);
        }

        private void CanListBoxDown(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SuggestionListBox != null && SuggestionListBox.ItemsSource != null && SuggestionListBox.SelectedIndex < SuggestionListBox.ItemsSource.OfType<object>().ToList().Count;
        }
        private void CanListBoxUp(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SuggestionListBox != null && SuggestionListBox.ItemsSource != null && SuggestionListBox.SelectedIndex > 0;
        }
    }
}
