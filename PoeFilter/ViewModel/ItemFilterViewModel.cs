﻿using System;
using PropertyChanged;

namespace PoeFilter.ViewModel
{
    public enum Rarities { None, Normal, Magic, Rare, Unique }
    public enum FilterVisibility { Hide, Show }

    [ImplementPropertyChanged]
    public class ItemFilterViewModel
    {
        public bool SetItemLevel { get; set; }
        public bool SetDropLevel { get; set; }
        public bool SetQuality { get; set; }
        public bool SetItemClass { get; set; }
        public bool SetBaseType { get; set; }
        public bool SetSocketGroup { get; set; }
        public bool SetSockets { get; set; }
        public bool SetLinkedSockets { get; set; }
        public bool SetRarity { get; set; }

        public FilterVisibility Visibility { get; set; }
        public int ItemLevel { get; set; }
        public int DropLevel { get; set; }
        public int Quality { get; set; }
        public String ItemClass { get; set; }
        public String BaseType { get; set; }
        public Rarities Rarities { get; set; }
        public String SocketGroup { get; set; }
        public int Sockets { get; set; }
        public int LinkedSockets { get; set; }

        public ItemFilterViewModel(ItemFilterViewModel itemFilter)
        {
            BaseType = itemFilter.BaseType;
            DropLevel = itemFilter.DropLevel;
            ItemClass = itemFilter.ItemClass;
            ItemLevel = itemFilter.ItemLevel;
            LinkedSockets = itemFilter.LinkedSockets;
            Quality = itemFilter.Quality;
            Rarities = itemFilter.Rarities;
            SetBaseType = itemFilter.SetBaseType;
            SetDropLevel = itemFilter.SetDropLevel;
            SetItemClass = itemFilter.SetItemClass;
            SetItemLevel = itemFilter.SetItemLevel;
            SetLinkedSockets = itemFilter.SetLinkedSockets;
            SetQuality = itemFilter.SetQuality;
            SetRarity = itemFilter.SetRarity;
            SetSocketGroup = itemFilter.SetSocketGroup;
            SetSockets = itemFilter.SetSockets;
            SocketGroup = itemFilter.SocketGroup;
            Sockets = itemFilter.Sockets;
            Visibility = itemFilter.Visibility;
        }
        public ItemFilterViewModel()
        {
            LinkedSockets = Sockets = Quality = DropLevel = ItemLevel = 0;
            ItemClass = BaseType = SocketGroup = String.Empty;
            Rarities = Rarities.None;
            Visibility = FilterVisibility.Show;
        }
    }
}
