﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using PropertyChanged;

namespace PoeFilter.ViewModel
{
    [ImplementPropertyChanged]
    public class ItemStyleViewModel
    {
        public Color BackgroundColor { get; set; }
        public Color TextColor { get; set; }
        public Color BorderColor { get; set; }
        public int AudioClip { get; set; }
        public bool SetBorderColor { get; set; }
        public bool SetBackgroundColor { get; set; }
        public bool SetTextColor { get; set; }
        public bool SetFontSize { get; set; }
        public double FontSize { get; set; }

        public ItemStyleViewModel(ItemStyleViewModel itemStyle)
        {
            AudioClip = itemStyle.AudioClip;
            BackgroundColor = itemStyle.BackgroundColor;
            BorderColor = itemStyle.BorderColor;
            TextColor = itemStyle.TextColor;
            SetBackgroundColor = itemStyle.SetBackgroundColor;
            SetBorderColor = itemStyle.SetBorderColor;
            SetTextColor = itemStyle.SetTextColor;
        }
        public ItemStyleViewModel()
        {
            BackgroundColor = Colors.Black;
            BorderColor = Colors.Black;
            TextColor = Colors.Black;
            AudioClip = 0;
            FontSize = 12;
            SetBorderColor = SetBackgroundColor = SetTextColor = false;
        }
    }
}
