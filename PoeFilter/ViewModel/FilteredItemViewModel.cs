﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

namespace PoeFilter.ViewModel
{
    [ImplementPropertyChanged]
    public class FilteredItemViewModel
    {
        public ItemStyleViewModel ItemStyle { get; set; }
        public ItemFilterViewModel ItemFilter { get; set; }

        public FilteredItemViewModel(FilteredItemViewModel filter)
        {
            ItemStyle = new ItemStyleViewModel(filter.ItemStyle);
            ItemFilter = new ItemFilterViewModel(filter.ItemFilter);
        }

        public FilteredItemViewModel()
        {
            ItemStyle = new ItemStyleViewModel();
            ItemFilter = new ItemFilterViewModel();
        }
    }
}