﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using Commander;
using PropertyChanged;

namespace PoeFilter.ViewModel
{
    [ImplementPropertyChanged]
    public class MainViewModel
    {
        public ObservableCollection<String> ItemClasses { get; set; }
        public ObservableCollection<String> BaseTypes { get; set; }

        public FilteredItemViewModel CurrentFilter { get; set; }

        public ObservableCollection<FilteredItemViewModel> ExampleItems { get; set; }

        public ObservableCollection<FilteredItemViewModel> SavedFilters { get; set; }

        public MainViewModel()
        {
            var startupLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (startupLocation == null) return;

            var itemClassDirectory = Path.Combine(startupLocation, "Model", "ItemClasses");
            var baseTypesDirectory = Path.Combine(startupLocation, "Model", "BaseTypes");

            CurrentFilter = new FilteredItemViewModel();
            ItemClasses = new ObservableCollection<string>();
            BaseTypes = new ObservableCollection<string>();
            SavedFilters = new ObservableCollection<FilteredItemViewModel>();
            ExampleItems = new ObservableCollection<FilteredItemViewModel>();

            //Load all ItemClasses from data files
            Directory.EnumerateFiles(itemClassDirectory)
                .SelectMany(dataFile => File.ReadAllLines(dataFile))
                .Distinct().ToList()
                .ForEach(itemClass => ItemClasses.Add(itemClass));

            //Load all BaseType from data files
            Directory.EnumerateFiles(baseTypesDirectory)
                .SelectMany(dataFile => File.ReadAllLines(dataFile))
                .Distinct().ToList()
                .ForEach(baseType => BaseTypes.Add(baseType));
        }

        [OnCommand("SaveFilterCommand")]
        public void SaveFilter()
        {
            SavedFilters.Add(new FilteredItemViewModel(CurrentFilter));
            CurrentFilter = new FilteredItemViewModel();
        }

        [OnCommand("EditFilterCommand")]
        public void EditFilter(FilteredItemViewModel filter) { CurrentFilter = filter; }

        [OnCommand("NewFilterCommand")]
        public void NewFilter() { CurrentFilter = new FilteredItemViewModel(); }

        [OnCommand("CloseCommand")]
        public void Close() { Application.Current.Shutdown(); }
    }
}
