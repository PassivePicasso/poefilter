﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PoeFilter.Hotkeys;

namespace PoeFilter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GlobalHotkey _showHideHotKey;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += (sender, args) =>
            {
                var src = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
                if (src != null) src.AddHook(WndProc);
                _showHideHotKey = new GlobalHotkey(Constants.CTRL + Constants.SHIFT, Keys.Enter, this);
                _showHideHotKey.Register();
            };
            Unloaded += (sender, args) => _showHideHotKey.Unregiser();
        }

        public IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {

            if (msg == Hotkeys.Constants.WM_HOTKEY_MSG_ID)
            {
                Visibility = Visibility == Visibility.Visible ? Visibility.Hidden : Visibility.Visible;
            }

            return IntPtr.Zero;
        }

        private void OuterEdgeClick(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
