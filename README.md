# Path of Exile Item Filter Manager #

### What is this repository for? ###

To Track development and bug reports for an Item Filter generator for the game [Path Of Exile](http://www.pathofexile.com)

### How do I get set up? ###

* Clone repository
* Enable NuGet package restore for solution in Visual Studio
* Build project

### Contribution guidelines ###

* Follow MVVM conventions
* Write clean concise code
* ReSharper cleanliness is preferred but not required.


### Attribution ###

Library              | Repository                                     | License
---------------------|------------------------------------------------|-----------
Fody                 | https://github.com/Fody/Fody                   | MIT
Commander.Fody       | https://github.com/DamianReeves/Commander.Fody | MIT
PropertyChanged.Fody | https://github.com/Fody/PropertyChanged        | MIT

### Who do I talk to? ###

* Repo owner or admin

### License ###
The MIT License (MIT)

Copyright (c) 2015 James LaPenn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.